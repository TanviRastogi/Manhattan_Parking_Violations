# -*- coding: utf-8 -*-
"""
Created on Tue May  9 22:13:10 2017

@author: tanvirastogi
"""

import pandas as pd
import math
import numpy as np
# Function1 calculates distance from empire estate building
def distfinder(lat2,lon2): 
    lat1 = 40.7484
    lon1 = -73.9857
    dist = math.acos(math.cos(math.radians(90-lat1))*math.cos(math.radians(90-lat2))+math.sin(math.radians(90-lat1))*math.sin(math.radians(90-lat2))*math.cos(math.radians(lon1-lon2)))*3959
    return dist
    
# function finds the zone for a location
def zonefinder(a):
    if a > 0 and a<=2 :
        zone = "zone1"
    elif a >2 and a<=4:
        zone = "zone2"
    elif a >4 and a<=6:
        zone = "zone3"   
    elif a >6 and a<=8:
        zone = "zone4"     
    else:
        zone = "zone5"
    
    return zone
    
  # function for getting fines    
def getfine(a): 
    dff = pd.read_csv("codefinedata.csv")
    pointer = dff[dff['CODE']== a].index.tolist()
    fine = dff.loc[pointer]['Fine']  
    return fine     
# function to get latitude   
def getlatitude(x):
    dflat = pd.read_csv("NYCdataFinalGeocoded100k.csv")
    dflat = dflat[["ZipCode","Latitude"]]
    dflat = dflat.loc[dflat['ZipCode']==x] 
    dflat = dflat.dropna()
    dflat= dflat.Latitude.unique()
    return dflat[0]


# function to get longitude
def getlongitude(x):
    dflat = pd.read_csv("NYCdataFinalGeocoded100k.csv")
    dflat = dflat[["ZipCode","Longitude"]]
    dflat = dflat.loc[dflat['ZipCode']==x] 
    dflat = dflat.dropna()
    dflat= dflat.Longitude.unique()
    return dflat[0]



# function to get top5 violation codes zonewise

def gettop10codeszone(df1,carmake):
    df1 = df1[df1.make == carmake]

#making pivot table as in excel  
    table1 =  pd.pivot_table(df1,index=['Zone'], columns=['code'],values = ['ticket'], aggfunc=len)

# converting pivot to dataframe 
    flattened1 = pd.DataFrame(table1.to_records()) 
#renaming columns of newdataframe made by pivot
    flattened1.columns = [hdr.replace("('ticket', ", "").replace(")", "") \
                     for hdr in flattened1.columns]
                   
    flattened1 = flattened1.set_index('Zone') # setting zone columns as index
    flattened1.fillna(0, inplace=True)   # removing Nan vlaues from data frame
    
# ranking the values of new dataframe created by pivot
    ranked_df1 = flattened1.rank(axis =1)
    numbrcol1 = len(ranked_df1.columns)
#Create truth matrix for top 5( 73-5 = 68)
    rank_gt_ten1 = ranked_df1 > numbrcol1-5
# dropping which has all false values
    filteredrank =  rank_gt_ten1.ix[:,(rank_gt_ten1 != 0).any(axis=0)]

# passing columnnames in a list
    colList = list(filteredrank.columns.values)
    new_df1 = flattened1[colList] # creating final dataframe with union of top 5 
    return(new_df1)    


# Chart for top10violation code(part1)
def gettop10codeszonechart(new_df2):
    my_plot = new_df2.plot.barh(stacked=True,title="No. of tickets by Viotation code per zone by car make")
    my_plot.set_xlabel("No. of tickets for Viotation code by carmake")
    return True

# function to get top5 violation codes on dollarvalue zonewise by carmake(part2)

def getdollarvalue(df2,carmake2):
    

    df2 = df2[df2.make == carmake2]
#making pivot table as in excel  
    table2 =  pd.pivot_table(df2,index=['Zone'], columns=['code'],values = ['Fine'], aggfunc=np.sum,fill_value=0)
#print(table1)
# converting pivot to dataframe 
    flattened2 = pd.DataFrame(table2.to_records()) 
#renaming columns of newdataframe made by pivot
    flattened2.columns = [hdr.replace("('Fine', ", "").replace(")", "") \
                     for hdr in flattened2.columns]
                   
    flattened2 = flattened2.set_index('Zone') # setting zone columns as index
#flattened.fillna(0, inplace=True)   # removing Nan vlaues from data frame


# ranking the values of new dataframe created by pivot
    ranked_df2 = flattened2.rank(axis =1)
    numbrcol2 = len(ranked_df2.columns)
#print(flattened)
#Create truth matrix for top 5( 73-5 = 68)
    rank_gt_ten2 = ranked_df2 > numbrcol2-5
# dropping which has all false values
    filteredrank2 =  rank_gt_ten2.ix[:,(rank_gt_ten2 != 0).any(axis=0)]

# passing columnnames in a list
    colList = list(filteredrank2.columns.values)
    new_df3 = flattened2[colList] # creating final dataframe with union of top 5 
    new_df3 = new_df3.divide(1000, axis='columns', level=None, fill_value=None) # converting $ value in 1000s
    print("Below is the data frame that we want to print in grid format with formatting")

    #new_df3 = new_df3.applymap('${:,.2f}'.format) #formating data in dollar form
    return(new_df3)

# Chart for dollarvalue dataframe(part2)
def getdollarcodeszonechart(new_df4):
    my_plot = new_df4.plot.barh(stacked=True,title="Dollar Value of Viotations per zone by car make")
    my_plot.set_xlabel("$ Value for Violation Codes")
    return True
    
def safetyfinder(dfs,userloc):
     tables =  pd.pivot_table(dfs,index=['Zone'], columns=['code'],values = ['ticket'], aggfunc=len)

# converting pivot to dataframe 
     flatteneds = pd.DataFrame(tables.to_records()) 
#renaming columns of newdataframe made by pivot
     flatteneds.columns = [hdr.replace("('ticket', ", "").replace(")", "") \
                     for hdr in flatteneds.columns]
                   
     flatteneds = flatteneds.set_index('Zone') # setting zone columns as index
     flatteneds.fillna(0, inplace=True)   # removing Nan vlaues from data frame
    
# ranking the values of new dataframe created by pivot
     ranked_dfs = flatteneds.rank(axis =1)

     lst1 = ranked_dfs.loc['zone1']# Contains count for each violation code for zone1
     lst2 = ranked_dfs.loc['zone2']# Contains count for each violation code for zone2
     lst3 = ranked_dfs.loc['zone3']# Contains count for each violation code for zone3
     lst4 = ranked_dfs.loc['zone4']# Contains count for each violation code for zone4
     lst5 = ranked_dfs.loc['zone5']# Contains count for each violation code for zone5
     lst1 = lst1.sort_values()# sorting each list
     lst2 = lst2.sort_values()
     lst3 = lst3.sort_values()
     lst4 = lst4.sort_values()
     lst5 = lst5.sort_values()

     colList1 = list(lst1.index.values)# now conatining violation codes in sorted order in each list
     colList2 = list(lst2.index.values)
     colList3 = list(lst3.index.values)
     colList4 = list(lst4.index.values)
     colList5 = list(lst5.index.values)
     colList1 = colList1[-1:]# getting top 1 code for zones
     colList2 = colList2[-1:]
     colList3 = colList3[-1:]
     colList4 = colList4[-1:]
     colList5 = colList5[-1:]	
	
     ulat = getlatitude(userloc)
     ulon = getlongitude(userloc)
     udist = distfinder(ulat,ulon)# finding ditance
     uzone = zonefinder(udist)# finding zone

     if uzone == 'zone1':
         a = colList1[0]
     elif uzone == 'zone2':
         a = colList2[0]
     elif uzone == 'zone3':
         a = colList3[0]
     elif uzone == 'zone4':
         a = colList4[0]  
     elif uzone == 'zone5':
         a = colList5[0]      

     a = int(a)
     ufine = getfine(a)
     ufine = ufine.values.tolist()
     desc = dfs.loc[df['code']==a]
     desc = desc.dropna()
     desc = desc[['code','description']]
     desc= desc.description.unique()

     print("You are in: " + uzone)
     print("You are most likely to get fine for violation code:" + str(a))
     print("Violation Description:" + desc[0])
     print("Your fine amount would be " + str(ufine[0]))  
        
     return True
   
#===================Intializing=============================================
    # using above functions and intializing with our data
df = pd.read_csv("NYCdataFinalGeocoded100k.csv")
df = df[["Summons Number","Vehicle Make","Violation Code","Violation Description","Latitude","Longitude","Fine"]] # selecting required columns- I added Fine column to our data
df.columns = ['ticket','make','code','description','lat','lon','Fine']#renaming dataframe columns
df = df[df.lat != 0]# removing unwanted data
df = df[df.lon != 0]# removing unwanted data

df.lat = df.lat.astype(float)
df.lon = df.lon.astype(float)
df['distance'] = list(map(distfinder,df['lat'],df['lon']))# finding distance for each of the rows in our dataframe and adding new column to dataframe
df['Zone']= list(map(zonefinder,df['distance'])) # finding zone for eachlocation and adding it to dataframe

#taking userinput for carmake to get top 5 violation codes(part1)

print("Violation code analysis by carmake")
model1 = input("Enter the car make")
codedf = gettop10codeszone(df,model1)
print(codedf)
chart1 = gettop10codeszonechart(codedf)

#taking userinput for carmake to get top 5 violation codes by dollar value (part2)
print("$ Valve analysis by carmake")
model2 = input("Enter the car make")
dollardf = getdollarvalue(df,model2)
dollardfp = dollardf.applymap('${:,.2f}'.format)
print(dollardfp)
chart2 = getdollarcodeszonechart(dollardf)


#taking userinput for finding zone and most likely fine and violation(part3)

print("Check your parking safety")
print("==============================")
Uloc  =  int(input("Enter your zipcode"))
park = safetyfinder(df,Uloc)




    